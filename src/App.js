import { Route, Switch } from 'react-router-dom'
import { version, Button } from 'antd'
import React from 'react'

import 'antd/dist/antd.css'

import Config from './Component/Config/index'
import Home from './Component/Home/index'
import Info from './Component/Info/index'
import Layout from './Component/Layout/index'

function App() {
  return (
    <Layout>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/config" component={Config} />
        <Route path="/info" component={Info} />
      </Switch>
    </Layout>
  )
}

export default App
