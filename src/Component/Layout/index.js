import { Layout } from 'antd'
import React, { Component } from 'react'

import Footer from './footer'
import Header from './header'

const { Content } = Layout
export default class Index extends Component {
  render() {
    console.log(this.props.children)
    return (
      <Layout>
        <Header />
        <Content> {this.props.children}</Content>
        <Footer />
      </Layout>
    )
  }
}
