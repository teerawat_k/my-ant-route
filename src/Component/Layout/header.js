import { Layout, Menu } from 'antd'
import { Link } from 'react-router-dom'
import React, { Component } from 'react'

const { Header } = Layout

const routes = [
  {
    key: '1',
    path: '/',
    name: 'Home'
  },
  {
    key: '2',
    path: '/info',
    name: 'Info'
  },
  {
    key: '3',
    path: '/config',
    name: 'Config'
  }
]

export default class Index extends Component {
  render() {
    return (
      <Header>
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['1']}
          style={{ lineHeight: '64px' }}
        >
          {routes.map(menu => (
            <Menu.Item key={menu.key}>
              <Link to={menu.path}> {menu.name}</Link>
            </Menu.Item>
          ))}
        </Menu>
      </Header>
    )
  }
}
